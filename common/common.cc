#include "common.hh"

#include <iostream>
#include <random>

void print_vec(std::vector<int>& vec) {
    int i = 0;
    std::cout << "[ ";
    while (i < vec.size() - 1) {
        std::cout << vec[i] << ", ";
        ++i;
    }
    std::cout << vec[i] << " ]" << std::endl;
}

std::vector<int> random_vector(int min, int max, int n_elements) {
    std::vector<int> rv;

    std::random_device rd;
    std::mt19937 generator(rd());
    std::uniform_int_distribution<int> distribution(min, max);

    for (int i = 0; i < n_elements; ++i) {
        int random_element = distribution(generator);
        rv.push_back(random_element);
    }

    return rv;
}