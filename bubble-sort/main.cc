#include <iostream>

void bubble_sort(int arr[], int size) {
    int min_idx = 0;
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size - i - 1; ++j) {
            if (arr[j + 1] < arr[j]) {
                int tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
            }
        }
    }
}

void print_arr(int arr[], int size) {
    int i = 0;
    printf("[");
    while (i < size - 1) {
        printf(" %d,", arr[i]);
        ++i;
    }
    printf(" %d ]\n", arr[i]);
}

#define size 15

int main() {
    int arr[size] = {7, 5, 9, 2, 8, 34, 3, 2, 23, 5, 6, 0, 88, 76, 4};

    print_arr(arr, size);
    bubble_sort(arr, size);
    print_arr(arr, size);

    return 0;
}