#include <iostream>
#include <vector>

struct Node {
    int val;
    Node* left;
    Node* right;
};

Node* new_node(int value) {
    Node* node = (Node*)malloc(sizeof(Node));
    node->val = value;
    node->left = nullptr;
    node->right = nullptr;

    return node;
}

void insert_left(Node* self, Node* left_node) { self->left = left_node; }

void insert_right(Node* self, Node* right_node) { self->right = right_node; }

// Preorder search: root, left, right
// Postorder search: left, right, root
// Inorder search: left, root, right

std::vector<int>& preorder_walk(Node* curr, std::vector<int>& path) {
    if (curr == nullptr) return path;

    path.push_back(curr->val);
    preorder_walk(curr->left, path);
    preorder_walk(curr->right, path);

    return path;
}

std::vector<int>& postorder_walk(Node* curr, std::vector<int>& path) {
    if (curr == nullptr) return path;

    postorder_walk(curr->left, path);
    postorder_walk(curr->right, path);
    path.push_back(curr->val);

    return path;
}

std::vector<int>& inorder_walk(Node* curr, std::vector<int>& path) {
    if (curr == nullptr) return path;

    inorder_walk(curr->left, path);
    path.push_back(curr->val);
    inorder_walk(curr->right, path);

    return path;
}

void preorder_search(Node* head, std::vector<int>& path) { preorder_walk(head, path); }

void inorder_search(Node* head, std::vector<int>& path) { inorder_walk(head, path); }

void postorder_search(Node* head, std::vector<int>& path) { postorder_walk(head, path); }

//----------------------------------------------------------------------------------------------//
// DFS
//----------------------------------------------------------------------------------------------//
#include <stack>

bool dfs_using_stack(Node* root) {
    std::stack<Node*> s;
    Node* curr = root;
    while (curr != nullptr || !s.empty()) {
        while (curr != nullptr) {
            s.push(curr);
            std::cout << curr->val << std::endl;
            curr = curr->left;
        }
        curr = s.top();
        s.pop();
        curr = curr->right;
    }
}

//----------------------------------------------------------------------------------------------//
// Compare two trees
//----------------------------------------------------------------------------------------------//

bool compare_tree(Node* a, Node* b) {
    if (a == nullptr && b == nullptr) {
        return true;
    }

    if (a == nullptr || a == nullptr) {
        return false;
    }

    if (a->val != b->val) {
        return false;
    }

    return compare_tree(a->left, a->right) && compare_tree(b->left, b->right);
}

//----------------------------------------------------------------------------------------------//
// Depth first find
//----------------------------------------------------------------------------------------------//

bool dff(Node* node, int val) {
    if (node == nullptr) {
        return false;
    }

    if (node->val == val) {
        return true;
    }

    if (node->val < val) {
        return dff(node->right, val);
    } else {
        return dff(node->left, val);
    }
}

int main(int argc, char** argv) {
    Node* a = new_node(1);
    Node* b = new_node(2);
    Node* c = new_node(3);
    Node* d = new_node(4);
    Node* e = new_node(5);
    Node* f = new_node(6);
    Node* g = new_node(7);

    insert_left(a, b);
    insert_left(b, d);
    insert_right(b, e);
    insert_right(a, c);
    insert_left(c, f);
    insert_right(c, g);

    dfs_using_stack(a);

    free(a);
    free(b);
    free(c);
    free(d);
    free(e);
    free(f);
    free(g);
}
