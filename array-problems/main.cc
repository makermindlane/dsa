#include <bits/stdc++.h>

#include "../common/common.hh"

int largest_element(std::vector<int>& arr);
int second_largest(std::vector<int>& arr);
bool is_array_sorted(std::vector<int>& arr);

void selection_sort(std::vector<int>& arr) {
    for (int i = 0; i < arr.size() - 1; ++i) {
        int min_idx = i;
        for (int j = i + 1; j < arr.size(); ++j) {
            if (arr[j] < arr[min_idx]) {
                min_idx = j;
            }
        }
        int tmp = arr[min_idx];
        arr[min_idx] = arr[i];
        arr[i] = tmp;
    }
}

int remove_duplicate(std::vector<int>& arr) {
    std::set<int> s;
    for (int i = 0; i < arr.size(); ++i) {
        s.insert(arr[i]);
    }

    int j = 0;
    for (int x : s) {
        arr[j] = x;
        ++j;
    }
    return s.size();
}

int main(int argc, char* argv[]) {
    int capacity = 10;

    if (argc == 2) {
        capacity = atoi(argv[1]);
    }
    std::cout << "Capacity: " << capacity << "\n";

    std::vector<int> arr = random_vector(1, capacity, capacity);

    // selection_sort(arr);
    print_vec(arr);
    if (is_array_sorted(arr)) {
        std::cout << "Sorted" << std::endl;
    } else {
        std::cout << "Not sorted" << std::endl;
    }

    return 0;
}
