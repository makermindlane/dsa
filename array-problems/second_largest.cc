#include <bits/stdc++.h>

int second_largest(std::vector<int>& arr) {
    std::sort(arr.begin(), arr.end());
    return arr[arr.size() - 2];
}
