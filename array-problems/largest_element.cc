#include <bits/stdc++.h>

int largest_element(std::vector<int>& arr) {
    std::sort(arr.begin(), arr.end());
    return arr[arr.size() - 1];
}
