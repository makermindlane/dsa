#include <bits/stdc++.h>

#include "../common/common.hh"
using namespace std;

void selection_sort(std::vector<int>& arr) {
    for (int i = 0; i < arr.size() - 1; ++i) {
        int min_idx = i;
        for (int j = i + 1; j < arr.size(); ++j) {
            if (arr[j] < arr[min_idx]) {
                min_idx = j;
            }
        }
        int tmp = arr[min_idx];
        arr[min_idx] = arr[i];
        arr[i] = tmp;
    }
}

int main() {
    std::vector<int> arr = random_vector(1, 20, 10);

    print_vec(arr);
    selection_sort(arr);
    print_vec(arr);

    return 0;
}
