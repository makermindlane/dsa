#include <iostream>

#include "../common/common.hh"

void insertion_sort(std::vector<int>& vec) {
    for (int i = 0; i < vec.size() - 1; ++i) {
        for (int j = i + 1; j > 0; --j) {
            if (vec[j - 1] < vec[j]) {
                break;
            }
            int tmp = vec[j - 1];
            vec[j - 1] = vec[j];
            vec[j] = tmp;
        }
    }
}

int main() {
    std::vector<int> v = random_vector(1, 10, 10);
    print_vec(v);
    insertion_sort(v);
    print_vec(v);
}
